// import {Fragment} from 'react'
import {useState} from 'react';
import {Container} from 'react-bootstrap'
import AppNavbar from './components/AppNavbar'
import {BrowserRouter as Router} from 'react-router-dom'
import {Route, Switch} from 'react-router-dom'
// import Banner from './components/Banner'
// import Highlights from './components/Highlights'
import Courses from './pages/Courses'
import Home from './pages/Home'
import Register from './pages/Register'
import Login from './pages/Login'
import Logout from './pages/Logout'
import PageNotFound from './pages/PageNotFound'
import './App.css';
import {UserProvider} from './UserContext';

function App() {

  // State hook for the user state that's define here for a global scope
  // Initialized as an object with properties from the localStorage
  // THis will be used to store the information and will be used for validating if user logged in on the app or not.
  const [user, setUser] = useState({
    email: localStorage.getItem('email')
  })

  // Function for clearing localStorage
  const unsetUser = () => {
    localStorage.clear();
  }

  // The UserProvider component is what allows other components to consume or use our context. Any component which is not wrapped by UserProvider will not have access to  the values provided for our context.

  // You can pass data or information to our context by providing a "value attribute in our UserProvider. Data passed here can be accessed by other components by unwrapping our context using the useContext hook.

  return (
    <UserProvider value={{user, setUser, unsetUser}} >
    <Router>
      <AppNavbar/>
      <Container>
        <Switch>
          <Route exact path="/" component={Home} />
          <Route exact path="/courses" component={Courses} />
          <Route exact path="/login" component={Login} />
          <Route exact path="/register" component={Register} />
          <Route exact path="/logout" component={Logout} />
          <Route component={PageNotFound} />
        </Switch>
      </Container>
    </Router>
    </UserProvider>
  );
}

export default App;


/*

ReacJS is a single page application(SPA). However, we can simulate the changing of pages. We don't actually create new pages, what we just do is switch pages according to their assigned routes. ReactJS and react-router-dom package just mimics or mirrors how HTML access its URL.

react-router-dom- 3 main components to simulate the changing of page

1. Router - wrapping the router component around other components will allow us to use routing within our page.

2. Switch - Allow us  to switch/ change our page components

3. Route - assigns a path which will trigger the change/switch of components render.

*/